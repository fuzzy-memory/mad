package com.example.newsomething;

import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.RotateAnimation;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Random;


public class MainActivity extends AppCompatActivity {
    Button mButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initui();
    }

    private void initui(){
        mButton = (Button) findViewById(R.id.button);
        registerForContextMenu(mButton);
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo){
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        menu.setHeaderTitle("Select an Action");
    }


    @Override
    public boolean onContextItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.opt1:
                Toast.makeText(this, "One", Toast.LENGTH_SHORT).show();
                break;
            case R.id.opt2:
                Toast.makeText(this, "Two", Toast.LENGTH_SHORT).show();
                break;
            case R.id.opt3:
                Toast.makeText(this, "Three", Toast.LENGTH_SHORT).show();
                break;
            default:
                return false;
        }
        return true;
    }
}