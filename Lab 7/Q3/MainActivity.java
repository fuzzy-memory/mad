package com.example.somethingelse;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;

import androidx.appcompat.app.AppCompatActivity;


public class MainActivity extends AppCompatActivity {
    ImageView mImage1, mImage2;
    int flag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initui();
    }

    private void initui(){
        mImage1 =(ImageView) findViewById(R.id.imageView);
        mImage2 =(ImageView) findViewById(R.id.imageView2);
        mImage1.setVisibility(View.INVISIBLE);
        mImage2.setVisibility(View.INVISIBLE);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater mi = getMenuInflater();
        mi.inflate(R.menu.menu_main,menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.item1:
                Toast.makeText(this,"Img1",Toast.LENGTH_SHORT).show();
                flag=1;
                break;
            case R.id.item2:
                Toast.makeText(this,"Img2",Toast.LENGTH_SHORT).show();
                flag=2;
                break;
        }
        img(flag);
        return true;
    }

    private void img(int flag){
        if(flag==1){
            mImage1.setVisibility(View.VISIBLE);
            mImage2.setVisibility(View.INVISIBLE);
            Toast.makeText(this, "Directions", Toast.LENGTH_SHORT).show();
        }
        if (flag==2){
            mImage1.setVisibility(View.INVISIBLE);
            mImage2.setVisibility(View.VISIBLE);
            Toast.makeText(this, "Flag", Toast.LENGTH_SHORT).show();
        }
    }

}