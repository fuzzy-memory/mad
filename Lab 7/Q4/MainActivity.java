package com.example.somethingelse;

import android.content.DialogInterface;
import android.os.Bundle;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.*;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;


public class MainActivity extends AppCompatActivity {
    TextView mText;
    String content, key;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        registerForContextMenu((TextView)findViewById(R.id.t1));
        initui();
    }

    private void initui(){
        mText =(TextView) findViewById(R.id.t1);
        content= mText.getText().toString();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater mi = getMenuInflater();
        mi.inflate(R.menu.menu_main,menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.item1:
                Toast.makeText(this,"Filtering",Toast.LENGTH_SHORT).show();
                filter();
                break;
            case R.id.item2:
                Toast.makeText(this,"Sorting",Toast.LENGTH_SHORT).show();
                sort();
                break;
        }
        return true;
    }

    private void filter(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Enter item");

        // Set up the input
        final EditText input = new EditText(this);
        input.setWidth(12);

        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                key = input.getText().toString();
                if (key.isEmpty()) {
                    Toast.makeText(MainActivity.this, "This field cannot be empty", Toast.LENGTH_SHORT).show();
                    return;
                }
                input.setText("");
                Toast.makeText(MainActivity.this, key, Toast.LENGTH_SHORT).show();
                findText();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();

    }

    private void findText(){
        int cnt=0;
        String[] splitted = content. split("\\s+");
        for(String xyz:splitted){
            if(xyz.equalsIgnoreCase(key))
                cnt++;
        }
        String disp="String '"+key+"' appeared "+cnt+" times";
        Toast.makeText(this, disp, Toast.LENGTH_SHORT).show();
    }

    private void sort(){
        StringBuilder abc=new StringBuilder(content);
        abc.reverse();
        content=abc.toString();
        mText.setText(content);
    }
}