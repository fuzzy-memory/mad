package com.example.newsomething;

import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Random;


public class MainActivity extends AppCompatActivity {
    ListView mList;
    String[] onPhone={"Phone", "SIM"};
    String[] conName={"Andy Bernard",
            "Angela Martin",
            "Creed Bratton",
            "Darryl Philbin",
            "Dwight Schrute",
            "Jan Levinson",
            "Jim Halpert",
            "Kelly Kapoor",
            "Kevin Malone",
            "Meredith Palmer",
            "Michael Scott",
            "Oscar Martinez",
            "Pam Beesly",
            "Phyllis Lapin",
            "Roy Anderson",
            "Ryan Howard",
            "Stanley Hudson",
            "Toby Flenderson"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initui();
    }

    private void initui(){
        mList=(ListView)findViewById(R.id.listView1);
        ArrayAdapter ad=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, conName);
        mList.setAdapter(ad);

        // Register the ListView  for Context menu
        registerForContextMenu(mList);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo){
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        menu.setHeaderTitle("Select an Action");
    }


    @Override
    public boolean onContextItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.opt1:
                call();
                break;
            case R.id.opt2:
                sms();
                break;
            case R.id.opt3:
                loc();
                break;
            case R.id.opt4:
                mailer();
                break;
            default:
                return false;
        }
        return true;
    }

    private void call(){ Toast.makeText(this, "Calling", Toast.LENGTH_SHORT).show(); }

    private void sms(){ Toast.makeText(this, "Texting", Toast.LENGTH_SHORT).show(); }

    private void loc(){
        Random rand = new Random();
        int n = rand.nextInt(2);
        Toast.makeText(this, onPhone[n], Toast.LENGTH_SHORT).show();
    }

    private void mailer(){
        Random rand = new Random();
        boolean x=rand.nextBoolean();
        String abc;
        if(x)
            abc="E-mail address exists";
        else abc="E-mail address not added";
        Toast.makeText(this, abc, Toast.LENGTH_SHORT).show();
    }
}