package com.abhiandroid.tablayoutexample;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;

public class MainActivity extends AppCompatActivity {

    FrameLayout mFrame;
    TabLayout mTab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    
        initui();
    }
    private void initui(){
        mFrame = (FrameLayout) findViewById(R.id.simpleFrameLayout);
        mTab = (TabLayout) findViewById(R.id.simpleTabLayout);
        
        TabLayout.Tab firstTab = mTab.newTab();
        firstTab.setText("Artists");
        mTab.addTab(firstTab);
        
        TabLayout.Tab secondTab = mTab.newTab();
        secondTab.setText("Albums");
        mTab.addTab(secondTab);

        TabLayout.Tab thirdTab = mTab.newTab();
        thirdTab.setText("Songs");
        mTab.addTab(thirdTab);


        // perform setOnTabSelectedListener event on TabLayout
        mTab.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                // get the current selected tab's position and replace the fragment accordingly
                Fragment mFrag = null;
                switch (tab.getPosition()) {
                    case 0:
                        mFrag = new FirstFragment();
                        break;
                    case 1:
                        mFrag = new SecondFragment();
                        break;
                    case 2:
                        mFrag = new ThirdFragment();
                        break;
                }
                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.simpleFrameLayout, mFrag);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                ft.commit();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

}
