//List view app
package com.example.some;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.*;

import java.util.*;

public class MainActivity extends AppCompatActivity {
    public static ListView mList;
    public ArrayList<String> listItems=new ArrayList<String>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initui();
    }

    private void initui() {
        mList = (ListView) findViewById(R.id.listView1);
        listgen();
        final ArrayAdapter ad = new ArrayAdapter(this, android.R.layout.simple_list_item_1, listItems);
        mList.setAdapter(ad);
    }

    private void listgen(){
        listItems.add("ABAP");
        listItems.add("ActionScript");
        listItems.add("Ada");
        listItems.add("ALGOL");
        listItems.add("Alice");
        listItems.add("APL");
        listItems.add("ASP / ASP.NET");
        listItems.add("Assembly Language");
        listItems.add("Awk");
        listItems.add("BBC Basic");
        listItems.add("C");
        listItems.add("C++");
        listItems.add("C#");
        listItems.add("COBOL");
        listItems.add("Cascading Style Sheets");
        listItems.add("D");
        listItems.add("Delphi");
        listItems.add("Dreamweaver");
        listItems.add("Erlang and Elixir");
        listItems.add("F#");
        listItems.add("FORTH");
        listItems.add("FORTRAN");
        listItems.add("Functional Programming");
        listItems.add("Go");
        listItems.add("Haskell");
        listItems.add("HTML");
        listItems.add("IDL");
        listItems.add("INTERCAL");
        listItems.add("Java");
        listItems.add("Javascript");
        listItems.add("jQuery");
        listItems.add("LabVIEW");
        listItems.add("Lisp");
        listItems.add("Logo");
        listItems.add("MetaQuotes Language");
        listItems.add("ML");
        listItems.add("Modula-3");
        listItems.add("MS Access");
        listItems.add("MySQL");
        listItems.add("NXT-G");
        listItems.add("Object-Oriented Programming");
        listItems.add("Objective-C");
        listItems.add("OCaml");
        listItems.add("Pascal");
        listItems.add("Perl");
        listItems.add("PHP");
        listItems.add("PL/I");
        listItems.add("PL/SQL");
        listItems.add("PostgreSQL");
        listItems.add("PostScript");
        listItems.add("PROLOG");
        listItems.add("Pure Data");
        listItems.add("Python");
        listItems.add("R");
        listItems.add("RapidWeaver");
        listItems.add("RavenDB");
        listItems.add("Rexx");
        listItems.add("Ruby on Rails");
        listItems.add("S-PLUS");
        listItems.add("SAS");
        listItems.add("Scala");
        listItems.add("Sed");
        listItems.add("SGML");
        listItems.add("Simula");
        listItems.add("Smalltalk");
        listItems.add("SMIL");
        listItems.add("SNOBOL");
        listItems.add("SQL");
        listItems.add("SQLite");
        listItems.add("SSI");
        listItems.add("Stata");
        listItems.add("Swift");
        listItems.add("Tcl/Tk");
        listItems.add("TeX and LaTeX");
        listItems.add("Unified Modeling Language");
        listItems.add("Unix Shells");
        listItems.add("Verilog");
        listItems.add("VHDL");
        listItems.add("Visual Basic");
        listItems.add("Visual FoxPro");
        listItems.add("VRML");
        listItems.add("WAP/WML");
        listItems.add("XML");
        listItems.add("XSL");
    }
}

