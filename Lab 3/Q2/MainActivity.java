//Grocery List app
package com.example.some;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.*;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    public static ListView mList;
    public String mText;
    final ArrayList<String> listItems = new ArrayList<String>();
    String starter="+ Add an item";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initui();
    }

    private void initui() {
        mList = (ListView) findViewById(R.id.listView1);
        listItems.add(starter);
        final ArrayAdapter ad = new ArrayAdapter(this, android.R.layout.simple_list_item_1, listItems);
        mList.setAdapter(ad);
        if(listItems.contains(starter))

        mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                if(listItems.contains(starter)) {
                    addfunc(view);
                    return;
                }
                Toast.makeText(MainActivity.this, listItems.get(position), Toast.LENGTH_SHORT).show();
            }
        });
        mList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view,
                                           int position, long arg3) {

                if(listItems.contains(starter)) {
                    Toast.makeText(MainActivity.this, "Nothing to remove", Toast.LENGTH_SHORT).show();
                    return false;
                }
                listItems.remove(position);
                ad.notifyDataSetChanged();
                if(listItems.isEmpty())
                    listItems.add(starter);
                Toast.makeText(MainActivity.this, "Item deleted", Toast.LENGTH_SHORT).show();

                return false;
            }

        });
    }

    public void addfunc(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Enter item");

        // Set up the input
        final EditText input = new EditText(this);
        input.setWidth(12);
        input.setTextColor(0x000000);

        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mText = input.getText().toString();
                if (mText.isEmpty()) {
                    Toast.makeText(MainActivity.this, "This field cannot be empty", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(listItems.contains(starter))
                    listItems.clear();
                listItems.add(mText);
                input.setText("");
                Toast.makeText(MainActivity.this, "Item added!", Toast.LENGTH_SHORT).show();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }
}
