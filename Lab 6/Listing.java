package com.example.newsomething;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class Listing extends AppCompatActivity {

    String[] courseList = {"AERO" , "CHE" , "EEE" , "ICE" , "AUTO" , "CIV" , "ECE" , "MECH" , "BIOMED" ,
            "CCE" , "MCT" , "CSE" , "IT" , "PMT"};
    String[] facultyList={"Mr. Akshay K C",
            "Anuradha Rao",
            "Anusha Hegde",
            "Arjun C V",
            "Chetana Pujari",
            "Chethan Sharma",
            "Diana Olivia",
            "Divya Rao",
            "Divya S",
            "Dr.Ajitha Shenoy K B",
            "Dr.Balachandra",
            "Dr.Chandrakala C B",
            "Dr.Manjula Shenoy K",
            "Dr.Manohara M M Pai",
            "Dr.Poornalatha G",
            "Dr.Preetham Kumar",
            "Dr.Radhika M Pai",
            "Dr.Raghavendra Achar",
            "Dr.Ramakrishna M",
            "Dr.Sanjay Singh",
            "Dr.Santhosha Rao",
            "Dr.Smitha N Pai",
            "Dr.Sucheta Kolekar",
            "Ghanashyama Prabhu",
            "Girija Attigeri",
            "Ipsita Upasana",
            "Jayashree",
            "K Rajesh Rao",
            "Dr. Krishna Prakasha K",
            "Manjula C Belavagi",
            "Mr.Akshay M J",
            "Ms.Aiswarya",
            "Ms.Anju R",
            "Nisha P Shetty",
            "Pooja S",
            "Raghavendra Ganiga",
            "Rashmi Naveen Raj",
            "Raviraja Holla M",
            "Sangeetha TS",
            "Santhosh Kamath",
            "Sirish Shetty B",
            "Smitha A",
            "Dr. Sumith N",
            "Swathi B P",
            "Tribikram Pradhan",
            "Veena K M",
            "Veena Mayya",
            "Vibha"};
    ListView mList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing);

        initui();
    }

    private void initui(){
        mList=(ListView) findViewById(R.id.listView1);
        ArrayAdapter ad=new ArrayAdapter(this, R.layout.listview_layout, facultyList);
        if(MainActivity.flag==1)
            ad=new ArrayAdapter(this, R.layout.listview_layout, courseList);
        if(MainActivity.flag==3)
            ad=new ArrayAdapter(this, R.layout.listview_layout, facultyList);
        mList.setAdapter(ad);
    }
}
