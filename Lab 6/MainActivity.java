package com.example.newsomething;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;


public class MainActivity extends AppCompatActivity {
    public static int flag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        registerForContextMenu((TextView)findViewById(R.id.t1));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater mi = getMenuInflater();
        mi.inflate(R.menu.menu_main,menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.item1:
                Toast.makeText(this,"Hello Courses",Toast.LENGTH_SHORT).show();
                flag=1;
                Intent mIntent1 = new Intent(this, Listing.class);
                startActivity(mIntent1);
                break;
            case R.id.item2:
                Toast.makeText(this,"Hello Admissions",Toast.LENGTH_SHORT).show();
                Intent mIntent2 = new Intent(this, Admit.class);
                startActivity(mIntent2);
                break;
            case R.id.item3:
                Toast.makeText(this,"Hello Faculty",Toast.LENGTH_SHORT).show();
                flag=3;
                Intent mIntent3 = new Intent(this, Listing.class);
                startActivity(mIntent3);
                break;
        }
        return true;
    }

    public void contact(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Contact us");
        builder.setMessage("Staff is on vacation");
        builder.show();
    }

    public void home(View view) {
        Toast.makeText(this, "You are already home", Toast.LENGTH_SHORT).show();
    }

    public void about(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("About us");
        builder.setMessage("Made in Manipal with <3");
        builder.show();
    }
}