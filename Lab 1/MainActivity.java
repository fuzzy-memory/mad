package com.example.helloworld;

import androidx.appcompat.app.*;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    int cnt=0;

    public void meth(View view) {
        Log.i("onClick", "onClick written to log");
        if(cnt>2){
            cnt=0;
            Toast.makeText(this,"Easy there",Toast.LENGTH_SHORT).show();
            cnt++;
            return;
        }
        Toast.makeText(this,"Check logcat",Toast.LENGTH_SHORT).show();
        cnt++;
    }
}