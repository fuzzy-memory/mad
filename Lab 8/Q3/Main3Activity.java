package com.example.androidstudioisasux;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import static com.example.androidstudioisasux.MainActivity.passMap;

public class Main3Activity extends AppCompatActivity {
    TextView mTextName, mTextYear, mTextStars, mTextReview;
    String name;
    dbHelper helperObject =new dbHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        initui();
    }

    private void initui(){
        mTextName = (TextView) findViewById(R.id.textView);
        mTextYear = (TextView) findViewById(R.id.TextView2);
        mTextStars = (TextView) findViewById(R.id.TextView4);
        mTextReview = (TextView) findViewById(R.id.TextView6);

        name=passMap.get("name");
        mTextName.setText(name);
        mTextYear.setText(passMap.get("year"));
        mTextStars.setText(passMap.get("stars"));
        mTextReview.setText(passMap.get("review"));

    }

    public void back(View view) {
        finish();
    }

    public void del(View view) {
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setTitle("Are you sure?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                helperObject.deleteItems(name);
                Toast.makeText(Main3Activity.this, name+" deleted", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
//                Toast.makeText(Main3Activity.this, "See ya", Toast.LENGTH_SHORT).show();
            }
        });
        builder.show();
    }
}
