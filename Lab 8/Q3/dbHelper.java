package com.example.androidstudioisasux;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.util.ArrayList;
import java.util.HashMap;


public class dbHelper extends SQLiteOpenHelper {
    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "usersdb";
    private static final String TABLE_Users = "userdetails";
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_YEAR = "year";
    private static final String KEY_STARS = "stars";
    private static final String KEY_REVIEW = "review";
    public dbHelper(Context context){
        super(context,DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db){
        String creation = "CREATE TABLE " + TABLE_Users + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_NAME + " TEXT,"
                + KEY_REVIEW + " TEXT,"
                + KEY_YEAR + " INTEGER,"
                + KEY_STARS + " INTEGER "+ ")";
        db.execSQL(creation);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        // Drop older table if exist
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_Users);
        // Create tables again
        onCreate(db);
    }
    // **** CRUD (Create, Read, Update, Delete) Operations ***** //

    // Adding new User Details
    void insertItems(String name, int year, int stars, String review){
        //Get the Data Repository in write mode
        SQLiteDatabase db = this.getWritableDatabase();
        //Create a new map of values, where column items are the keys
        ContentValues cValues = new ContentValues();
        cValues.put(KEY_NAME, name);
        cValues.put(KEY_YEAR, year);
        cValues.put(KEY_STARS, stars);
        cValues.put(KEY_REVIEW, review);
        // Insert the new row, returning the primary key value of the new row
        long newRowId = db.insert(TABLE_Users,null, cValues);
        db.close();
    }
    // Get User Details
    public ArrayList<HashMap<String, String>> getItems(){
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<HashMap<String, String>> userList = new ArrayList<>();
        String query = "SELECT name, year, stars, review FROM "+ TABLE_Users+" order by name";
        Cursor cursor = db.rawQuery(query,null);
        while (cursor.moveToNext()){
            HashMap<String,String> user = new HashMap<>();
            user.put("name",cursor.getString(cursor.getColumnIndex(KEY_NAME)));
            user.put("stars",cursor.getString(cursor.getColumnIndex(KEY_STARS)));
            user.put("year",cursor.getString(cursor.getColumnIndex(KEY_YEAR)));
            user.put("review", cursor.getString(cursor.getColumnIndex(KEY_REVIEW)));
            userList.add(user);
        }
        return  userList;
    }

    // Delete User Details
    public void deleteItems(String userid){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_Users, KEY_NAME +" = ?",new String[]{userid});
        db.close();
    }
    // Update User Details
    public void updateDetails(String title, int stars, String review){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cVals=new ContentValues();
        cVals.put(KEY_STARS, stars);
        cVals.put(KEY_REVIEW, review);
        db.update(TABLE_Users, cVals, KEY_ID+" = ?", new String[]{title});
    }
}