package com.example.androidstudioisasux;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.HashMap;


public class MainActivity extends AppCompatActivity {
    ListView mList;
    dbHelper helperObj = new dbHelper(this);
    public static HashMap<String, String> passMap;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initui();
    }

    @Override
    protected void onResume(){
        super.onResume();
        refresh();
    }

    private void initui(){
        mList=(ListView) findViewById(R.id.listView1);
        refresh();
        mList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener(){
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                viewing();
                passMap = (HashMap<String, String>) mList.getItemAtPosition(position);
                return true;
            }
        });
//        helperObj.insertItems("Pulp Fiction", 1994, 4, "GG\nBahut mazedaar\nMaa chod di bhai\nLovely");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.item1:
                add();
                break;
            default:
                return false;
        }
        return true;
    }

    private void add(){
        Intent mIntent=new Intent(this, Main2Activity.class);
        startActivity(mIntent);
//        Toast.makeText(this, "Adding", Toast.LENGTH_SHORT).show();

    }

    public void refresh(){
//        Toast.makeText(this, "Refreshing", Toast.LENGTH_SHORT).show();
        ArrayList<HashMap<String, String>> userList = helperObj.getItems();
        ListAdapter ad = new SimpleAdapter(MainActivity.this, userList, R.layout.listview_layout
                ,new String[]{"name","year","stars", "review"}, new int[]{R.id.textView1, R.id.textView2, R.id.textView3});
        mList.setAdapter(ad);
    }

    private void viewing(){
        Intent mIntent=new Intent(MainActivity.this, Main3Activity.class);
        startActivity(mIntent);
    }
}