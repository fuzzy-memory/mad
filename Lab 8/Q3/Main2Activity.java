package com.example.androidstudioisasux;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class Main2Activity extends AppCompatActivity {
    EditText mText1, mText2, mText3, mText4;
    dbHelper halp=new dbHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        initui();
    }

    private void initui(){
        mText1=(EditText) findViewById(R.id.editText);
        mText2=(EditText) findViewById(R.id.editText2);
        mText3=(EditText) findViewById(R.id.editText3);
        mText4=(EditText) findViewById(R.id.editText4);
    }

    public void add(View view) {
        if(mText1.getText().toString().isEmpty() || mText2.getText().toString().isEmpty() || mText3.getText().toString().isEmpty()){
            Toast.makeText(this, "Title, Year and Rating is mandatory", Toast.LENGTH_SHORT).show();
            return;
        }
        if(Integer.parseInt(mText3.getText().toString())>5) {
            Toast.makeText(this, "Stars cannot be more than 5", Toast.LENGTH_SHORT).show();
            return;
        }
        if(mText4.getText().toString().equals(""))
            mText4.setText("-");
        halp.insertItems(mText1.getText().toString().trim(), Integer.parseInt(mText2.getText().toString().trim()), Integer.parseInt(mText3.getText().toString().trim()), mText4.getText().toString().trim());
        Toast.makeText(this, "Damn gg", Toast.LENGTH_SHORT).show();
        finish();
    }
}
