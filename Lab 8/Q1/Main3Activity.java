package com.example.somethingelse;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import static com.example.somethingelse.MainActivity.passMap;

public class Main3Activity extends AppCompatActivity {
    TextView mText1;
    EditText mText2, mText3;
    String n, p, e;
    Button mButtonConf, mButtonEdit;
    dbHelper helperObj = new dbHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        initui();
    }

    private void initui() {
        mText1 = (TextView) findViewById(R.id.editText);
        mText2 = (EditText) findViewById(R.id.editText2);
        mText3 = (EditText) findViewById(R.id.editText4);
        mButtonConf = (Button) findViewById(R.id.button5);
        mButtonEdit=(Button) findViewById(R.id.button3);

        n = passMap.get("name");
        p = passMap.get("ph");
        e = passMap.get("email");

        mText1.setText(n);
        mText2.setText(p);
        mText3.setText(e);

        mText2.setEnabled(false);
        mText3.setEnabled(false);

        mButtonConf.setVisibility(View.INVISIBLE);
    }

    public void del(View view) {
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setTitle("Are you sure?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dele();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    public void conf(View view) {
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setTitle("Are you sure?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                editmax();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    public void bye(View view) {
        finish();
    }

    public void editor(View view) {
        mButtonConf.setVisibility(View.VISIBLE);
        mButtonEdit.setVisibility(View.INVISIBLE);
        mText2.setEnabled(true);
        mText3.setEnabled(true);
    }

    private void editmax(){
        if(mText2.getText().toString().isEmpty()|| mText3.getText().toString().isEmpty()){
            Toast.makeText(this, "All fields are mandatory", Toast.LENGTH_SHORT).show();
            return;
        }
        helperObj.UpdateUserDetails(mText2.getText().toString(), mText3.getText().toString(), mText1.getText().toString());
        Toast.makeText(this, "Success", Toast.LENGTH_SHORT).show();
        mText2.setEnabled(false);
        mText3.setEnabled(false);
    }

    private void dele(){
        helperObj.DeleteUser(mText1.getText().toString());
        Toast.makeText(this, "Contact deleted successfully", Toast.LENGTH_SHORT).show();
        finish();
    }
}