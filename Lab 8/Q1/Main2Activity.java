package com.example.somethingelse;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class Main2Activity extends AppCompatActivity {
    EditText mText1, mText2, mText3;
    dbHelper helperObj=new dbHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        initui();
    }

    private void initui(){
        mText1=(EditText) findViewById(R.id.editText);
        mText2=(EditText) findViewById(R.id.editText2);
        mText3=(EditText) findViewById(R.id.editText3);
    }

    public void add(View view){
        String name=mText1.getText().toString();
        String ph=mText2.getText().toString();
        String email=mText3.getText().toString();
        if(name.equals("") || ph.equals("") || email.equals("")){
            Toast.makeText(this, "All fields are mandatory", Toast.LENGTH_SHORT).show();
            return;
        }
        helperObj.insertUserDetails(name, ph, email);
        Toast.makeText(this, "Contact added", Toast.LENGTH_SHORT).show();
        finish();
    }

}
