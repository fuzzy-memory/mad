package com.example.alloveragain;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

public class Main2Activity extends AppCompatActivity {
    String items[]={"Select",
            "Bread",
            "Bananas",
            "Canned Beans",
            "Cheese",
            "Cornflakes",
            "Dairy",
            "Eggs",
            "Frozen Peas",
            "Meat",
            "Oil",
            "Pasta",
            "Pepper",
            "Rice",
            "Salad Dressing",
            "Sauce",
            "Seafood"};
    int itemCost[]={0, 35, 20, 300, 55, 120, 23, 30, 150, 300, 195, 300, 60, 500, 120, 70, 500};
    Spinner mSpin;
    EditText mText1;
    TextView mText2;
    dbHelper halp=new dbHelper(Main2Activity.this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        initui();
    }

    private void initui(){
        mSpin=(Spinner) findViewById(R.id.spinner);
        mText1=(EditText) findViewById(R.id.editText);
        mText2=(TextView) findViewById(R.id.textView5);

        ArrayAdapter ad=new ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, items);
        mSpin.setAdapter(ad);

        mSpin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                Toast.makeText(Main2Activity.this, "Yay", Toast.LENGTH_SHORT).show();
                mText2.setText(itemCost[position]+"");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void add(View view) {
        String item = mSpin.getSelectedItem().toString();
        if (item.equals("Select") || mText1.getText().toString().equals("")) {
            Toast.makeText(this, "All fields are mandatory", Toast.LENGTH_SHORT).show();
            return;
        }
        int qty = Integer.parseInt(mText1.getText().toString());
        if(qty==0){
            Toast.makeText(this, "Quantity can't be 0", Toast.LENGTH_SHORT).show();
            return;
        }
        boolean c=check(item);
        if(c)
            return;
        int price=itemCost[mSpin.getSelectedItemPosition()]*qty;
        halp.insertItems(item, qty, price);
        String da="units";
        if(qty==1)
            da="unit";
        String xyz=qty+" "+ da +" of "+item+" added";
        Toast.makeText(this, xyz, Toast.LENGTH_SHORT).show();
        finish();
    }

    private boolean check(String item){
        ArrayList<HashMap<String, String>> userList = halp.getItems();
        for(int i=0; i<userList.size(); i++){
            HashMap<String, String> passMap=userList.get(i);
            if(passMap.get("item").equals(item)) {
                int newqty=Integer.parseInt(passMap.get("qty"))+Integer.parseInt(mText1.getText().toString());
                int price=itemCost[mSpin.getSelectedItemPosition()]*newqty;
                halp.UpdateUserDetails(newqty, price, item);
                String da="units";
                if(Integer.parseInt(mText1.getText().toString())==1)
                    da="unit";
                String xyz=item+" already exists, incrementing quantity by "+mText1.getText().toString()+" "+da;
                Toast.makeText(this, xyz, Toast.LENGTH_SHORT).show();
                finish();
                return true;
            }
        }
        return false;
    }
}
