package com.example.alloveragain;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.HashMap;


public class MainActivity extends AppCompatActivity {
    ListView mList;
    dbHelper helperObj = new dbHelper(this);
    public static HashMap<String, String> passMap;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initui();
    }

    @Override
    protected void onResume(){
        super.onResume();
        refresh();
    }

    private void initui(){
        mList=(ListView) findViewById(R.id.listView1);
        refresh();
        mList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener(){
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                viewing();
                passMap = (HashMap<String, String>) mList.getItemAtPosition(position);
                return true;
            }
        });
//        helperObj.insertItems("Cheese", 2, 200);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.item1:
                add();
                break;
            default:
                return false;
        }
        return true;
    }

    private void add(){
        Intent mIntent=new Intent(this, Main2Activity.class);
        startActivity(mIntent);

    }

    public void refresh(){
        ArrayList<HashMap<String, String>> userList = helperObj.getItems();
        ListAdapter ad = new SimpleAdapter(MainActivity.this, userList, R.layout.listview_layout
                ,new String[]{"item","qty","price"}, new int[]{R.id.textView1, R.id.textView2, R.id.textView3});
        mList.setAdapter(ad);
    }

    private void viewing(){
//        Intent mIntent=new Intent(MainActivity.this, Main3Activity.class);
//        startActivity(mIntent);
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to delete this item?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String id=passMap.get("item");
                helperObj.deleteItems(id);
                refresh();
//                Toast.makeText(MainActivity.this, "Cool "+id, Toast.LENGTH_SHORT).show();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    public void total(View view) {
        String xyz="Total amount payable = "+helperObj.totalcost();
        Toast.makeText(this, xyz, Toast.LENGTH_SHORT).show();
    }
}