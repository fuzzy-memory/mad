package com.example.alloveragain;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.util.ArrayList;
import java.util.HashMap;


public class dbHelper extends SQLiteOpenHelper {
    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "usersdb";
    private static final String TABLE_Users = "userdetails";
    private static final String KEY_ID = "id";
    private static final String KEY_ITEM = "item";
    private static final String KEY_QTY = "qty";
    private static final String KEY_PRICE = "price";
    public dbHelper(Context context){
        super(context,DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db){
        String creation = "CREATE TABLE " + TABLE_Users + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_ITEM + " TEXT,"
                + KEY_QTY + " INTEGER,"
                + KEY_PRICE + " INTEGER "+ ")";
        db.execSQL(creation);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        // Drop older table if exist
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_Users);
        // Create tables again
        onCreate(db);
    }
    // **** CRUD (Create, Read, Update, Delete) Operations ***** //

    // Adding new User Details
    void insertItems(String item, int qty, int price){
        //Get the Data Repository in write mode
        SQLiteDatabase db = this.getWritableDatabase();
        //Create a new map of values, where column items are the keys
        ContentValues cValues = new ContentValues();
        cValues.put(KEY_ITEM, item);
        cValues.put(KEY_QTY, qty);
        cValues.put(KEY_PRICE, price);
        // Insert the new row, returning the primary key value of the new row
        long newRowId = db.insert(TABLE_Users,null, cValues);
        db.close();
    }
    // Get User Details
    public ArrayList<HashMap<String, String>> getItems(){
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<HashMap<String, String>> userList = new ArrayList<>();
        String query = "SELECT item, qty, price FROM "+ TABLE_Users;
        Cursor cursor = db.rawQuery(query,null);
        while (cursor.moveToNext()){
            HashMap<String,String> user = new HashMap<>();
            user.put("item",cursor.getString(cursor.getColumnIndex(KEY_ITEM)));
            user.put("price",cursor.getString(cursor.getColumnIndex(KEY_PRICE)));
            user.put("qty",cursor.getString(cursor.getColumnIndex(KEY_QTY)));
            userList.add(user);
        }
        return  userList;
    }
    // Get User Details based on userid
    public ArrayList<HashMap<String, String>> GetUserByUserId(int userid){
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<HashMap<String, String>> userList = new ArrayList<>();
        String query = "SELECT item, qty, price FROM "+ TABLE_Users;
        Cursor cursor = db.query(TABLE_Users, new String[]{KEY_ITEM, KEY_QTY, KEY_PRICE}, KEY_ID+ "=?",new String[]{String.valueOf(userid)},null, null, null, null);
        if (cursor.moveToNext()){
            HashMap<String,String> user = new HashMap<>();
            user.put("item",cursor.getString(cursor.getColumnIndex(KEY_ITEM)));
            user.put("price",cursor.getString(cursor.getColumnIndex(KEY_PRICE)));
            user.put("qty",cursor.getString(cursor.getColumnIndex(KEY_QTY)));
            userList.add(user);
        }
        return  userList;
    }
    // Delete User Details
    public void deleteItems(String userid){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_Users, KEY_ITEM +" = ?",new String[]{userid});
        db.close();
    }
    // Update User Details
    public int UpdateUserDetails(int qty, int price, String id){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cVals = new ContentValues();
        cVals.put(KEY_QTY, qty);
        cVals.put(KEY_PRICE, price);
        int count = db.update(TABLE_Users, cVals, KEY_ITEM +" = ?",new String[]{id});
        return  count;
    }

    public int get_id(String key) {
        SQLiteDatabase db = this.getWritableDatabase();
        int xyz = -1;
        Cursor cursor = db.rawQuery("select " + KEY_ID + " from " + TABLE_Users + " where item ='" + key + "'", null);
        while (cursor.moveToNext())
            xyz = Integer.parseInt(cursor.getString(cursor.getColumnIndex(KEY_ITEM)));
        return xyz;
    }
    public String totalcost(){
        SQLiteDatabase db=this.getWritableDatabase();
        int abc=0;
        Cursor curse=db.rawQuery("select "+KEY_PRICE+" from "+TABLE_Users, null);
        while (curse.moveToNext())
            abc+= Integer.parseInt(curse.getString(curse.getColumnIndex(KEY_PRICE)));
        return abc+"";
        }
}