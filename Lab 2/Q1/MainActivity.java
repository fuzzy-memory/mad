package com.example.lab2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void actionclick(View view) {
        TextView textView2=(TextView) findViewById(R.id.textView2);
        EditText editText1=(EditText)findViewById(R.id.editText1);
        if(editText1.getText().toString().equals("")) {
            textView2.setText("Enter text");
            return;
        }
        textView2.setText("My favorite subject is "+editText1.getText());
    }
}
