package com.example.q2;

import androidx.appcompat.app.AppCompatActivity;

import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void disapp(View view) {
        ImageView imageView1=(ImageView) findViewById(R.id.imageView1);
        imageView1.setVisibility(View.INVISIBLE);
    }
}
