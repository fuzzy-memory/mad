package com.example.some;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {
    private TextView t1;
    private TextView t2;
    private TextView t3;
    private TextView t4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        initUI();
    }

    private void initUI(){
        t1=(TextView) findViewById(R.id.textView2_1);
        t2=(TextView) findViewById(R.id.textView2_2);
        t3=(TextView) findViewById(R.id.textView2_3);
        t4=(TextView) findViewById(R.id.textView2_4);

        t1.setText(MainActivity.onepass+"");
        t2.setText(MainActivity.twopass+"");
        switch ((int) MainActivity.oppass){
            case 1:
                t3.setText("+");
                break;
            case 2:
                t3.setText("-");
                break;
            case 3:
                t3.setText("X");
                break;
            case 4:
                t3.setText("/");
                break;
            default:
                t3.setText("Error");
        }
        t4.setText(MainActivity.respass+"");
    }

    public void back(View view) {
        finish();
    }
}

