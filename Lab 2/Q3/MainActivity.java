//Calculator app
package com.example.some;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.*;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    private EditText mValueOne;
    private EditText mValueTwo;
    private TextView operator;
    private float res;

    public static float onepass=0;
    public static float twopass=0;
    public static float oppass=-1;
    public static float respass=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initUI();

    }
    private void initUI(){
        mValueOne = (EditText) findViewById(R.id.editText1);
        mValueTwo = (EditText) findViewById(R.id.editText2);
        operator =(TextView) findViewById(R.id.textView1);
        res=0;
    }

    public float getValueOne() {
        try {
            if (mValueOne.getText().toString().isEmpty())
                onepass=0;
            else
                onepass=Float.valueOf(mValueOne.getText().toString());
        }
        catch (Exception ex){
            Toast.makeText(this, "Error"+ex.getStackTrace().toString(), Toast.LENGTH_SHORT).show();
        }
        return onepass;
    }
    public float getValueTwo() {
        try {
            if (mValueTwo.getText().toString().isEmpty())
                twopass = 0;
            else
                twopass=Float.valueOf(mValueTwo.getText().toString());
        }
        catch (Exception ex){
            Toast.makeText(this, "Error"+ex.getStackTrace().toString(), Toast.LENGTH_SHORT).show();
        }
        return twopass;
    }
    public void add(View view) {
        res=getValueOne()+getValueTwo();
        operator.setText("+");
        oppass=1;
        respass=res;

//        Toast.makeText(this, res+"", Toast.LENGTH_SHORT).show();
    }

    public void sub(View view) {
        res=getValueOne()-getValueTwo();
        operator.setText("-");
        oppass=2;
        respass=res;
//        Toast.makeText(this, res+"", Toast.LENGTH_SHORT).show();
    }

    public void mul(View view) {
        res=getValueOne()*getValueTwo();
        operator.setText("X");
        oppass=3;
        respass=res;
//        Toast.makeText(this, res+"", Toast.LENGTH_SHORT).show();
    }

    public void div(View view) {
        res=getValueOne()/getValueTwo();
        operator.setText("/");
        oppass=4;
        respass=res;
//        Toast.makeText(this, res+"", Toast.LENGTH_SHORT).show();
    }

    public void eq(View view) {
        Intent intent = new Intent(this, Main2Activity.class);
        startActivity(intent);
        // Toast.makeText(this, res+"", Toast.LENGTH_SHORT).show();
    }

    public void clr(View view) {
        onepass=0;
        twopass=0;
        oppass=-1;
        respass=0;
        mValueOne.setText("");
        mValueTwo.setText("");
        operator.setText("");
    }
}