//Relative Layout
package com.example.some;


import android.os.Bundle;
import android.view.View;
import android.widget.*;


import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    private EditText t1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        t1=(EditText) findViewById(R.id.editText1);
    }

    public void cancel(View view) {
        Toast.makeText(this, "Oops", Toast.LENGTH_SHORT).show();
        t1.setText("");
    }

    public void okay(View view) {
        Toast.makeText(this, "Nice", Toast.LENGTH_LONG).show();
        t1.setText("");
    }
}