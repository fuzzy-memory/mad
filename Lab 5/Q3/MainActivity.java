package com.example.newsomething;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextPaint;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CalendarView;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity {
    public static Spinner mSpin1, mSpin2;
    public static DatePicker mDatePick;
    public static ToggleButton mToggle;
    public static TimePicker mTimePick;
    String[] dest={"Select", "BOM", "DEL", "BLR", "CCU", "HYD", "IXE"};
    String date=new String();
    int day, month, year, hr, min;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initui();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void initui(){
        mSpin1=(Spinner) findViewById(R.id.spinner1);
        mSpin2=(Spinner) findViewById(R.id.spinner2);
        mDatePick=(DatePicker) findViewById(R.id.datePicker);
        mToggle=(ToggleButton) findViewById(R.id.toggleButton);
        mTimePick=(TimePicker) findViewById(R.id.timePicker1);

        ArrayAdapter ad=new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, dest);
        mSpin1.setAdapter(ad);
        mSpin2.setAdapter(ad);

        mToggle.setTextOff("General");
        mToggle.setTextOn("Tatkal");

        day=mDatePick.getDayOfMonth();
        month=mDatePick.getMonth();
        year=mDatePick.getYear();
        hr=mTimePick.getHour();
        min=mTimePick.getMinute();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void clear(View view) {
        mSpin1.setSelection(0);
        mSpin2.setSelection(0);
        mToggle.setChecked(false);
        mDatePick.updateDate(year, month, day);
        mTimePick.setHour(hr);
        mTimePick.setMinute(min);
    }

    public void submit(View view) {
        if (mSpin1.getSelectedItem().toString().equals("Select") || mSpin2.getSelectedItem().toString().equals("Select")){
            Toast.makeText(this, "All fields are mandatory", Toast.LENGTH_SHORT).show();
            return;
        }
        if(mSpin1.getSelectedItem().toString().equals(mSpin2.getSelectedItem().toString())){
            Toast.makeText(this, "Point of origin and destination cannot be the same", Toast.LENGTH_SHORT).show();
            return;
        }
        if(mToggle.isChecked()&&hr<11){
            Toast.makeText(this, "Tatkal booking is not allowed before 11 am", Toast.LENGTH_SHORT).show();
            return;
        }

        Intent inte=new Intent(this, Main2Activity.class);
        startActivity(inte);
    }
}
