package com.example.newsomething;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {
    TextView mText1, mText2, mText3, mText4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        initui();
    }

    private void initui(){
        mText1=(TextView) findViewById(R.id.textView);
        mText2=(TextView) findViewById(R.id.textView2);
        mText3=(TextView) findViewById(R.id.textView3);
        mText4=(TextView) findViewById(R.id.textView4);

        mText1.setText(mText1.getText().toString()+" "+MainActivity.mSpin1.getSelectedItem().toString());
        mText2.setText(mText2.getText().toString()+" "+MainActivity.mSpin2.getSelectedItem().toString());
        mText4.setText(mText4.getText().toString()+" "+MainActivity.mDatePick.getDayOfMonth()+"/"+MainActivity.mDatePick.getMonth()
                +"/"+MainActivity.mDatePick.getYear());
        mText3.setText(mText3.getText().toString()+" "+MainActivity.mToggle.getText().toString());
    }
}
