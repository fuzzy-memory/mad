package com.example.something;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.*;

public class Main2Activity extends AppCompatActivity {
    EditText m2Text1, m2Text2;
    Spinner m2Spin;
    Button mButton;
    static int srno=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        initui();
    }

    private void initui(){
        m2Text1=(EditText) findViewById(R.id.editText1);
        m2Text2=(EditText) findViewById(R.id.editText2);
        m2Spin=(Spinner)findViewById(R.id.spinner1);
        mButton=(Button) findViewById(R.id.button2);

        m2Text1.setText(MainActivity.mText1.getText().toString());
        m2Text2.setText(MainActivity.mText2.getText().toString());

        ArrayAdapter ad = new ArrayAdapter(this,
                android.R.layout.simple_spinner_dropdown_item,
                MainActivity.opt);
        m2Spin.setAdapter(ad);
        m2Spin.setSelection(MainActivity.mSpin.getSelectedItemPosition());

        m2Text1.setEnabled(false);
        m2Text2.setEnabled(false);
        m2Spin.setEnabled(false);
    }

    public void conf(View view) {
        if(m2Text1.getText().toString().isEmpty() || m2Text2.getText().toString().isEmpty() || m2Spin.getSelectedItem().toString().equals("Select")){
            Toast.makeText(this, "All fields are mandatory", Toast.LENGTH_LONG).show();
            return;
        }
        m2Text1.setEnabled(false);
        m2Text2.setEnabled(false);
        m2Spin.setEnabled(false);
        String disp="Serial number: "+ srno++ +"\nVehicle number: "+m2Text1.getText()+"\nRC Number: "+m2Text2.getText()+"\nVehicle" +
                " type: "+m2Spin.getSelectedItem();
        Toast.makeText(this, disp, Toast.LENGTH_SHORT).show();
        MainActivity.clear();
        finish();
    }

    public void edit(View view) {
        m2Text1.setEnabled(true);
        m2Text2.setEnabled(true);
        m2Spin.setEnabled(true);
        mButton.setVisibility(View.INVISIBLE);

    }
}
