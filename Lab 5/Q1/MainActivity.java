package com.example.something;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.*;

import androidx.appcompat.app.AppCompatActivity;


public class MainActivity extends AppCompatActivity {
    public static EditText mText1, mText2;
    public static Spinner mSpin;
    public static String opt[]={"Select", "Bike", "Car", "Truck", "SUV"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initui();
    }

    private void initui(){
        mSpin=(Spinner) findViewById(R.id.spinner1);
        mText1=(EditText) findViewById(R.id.editText1);
        mText2=(EditText) findViewById(R.id.editText2);
        ArrayAdapter ad = new ArrayAdapter(this,
                android.R.layout.simple_spinner_dropdown_item,
                opt);
        mSpin.setAdapter(ad);
    }

    public void submit(View view) {
        if(mText1.getText().toString().isEmpty() || mText2.getText().toString().isEmpty() || mSpin.getSelectedItem().toString().equals("Select")){
            Toast.makeText(this, "All fields are mandatory", Toast.LENGTH_LONG).show();
            return;
        }
        Intent mIntent = new Intent(MainActivity.this,
                Main2Activity.class);
        startActivity(mIntent);
    }

    public static void clear(){
        mText1.setText("");
        mText2.setText("");
        mSpin.setSelection(0);
    }

}