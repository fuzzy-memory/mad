package com.example.something;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    String[] versions = new String[] {
            "Android 10",
            "Android Pie",
            "Android Oreo",
            "Android Nougat",
            "Android Marshmallow",
            "Android Lollipop",
            "Android Kitkat",
            "Android Jellybean",
            "Android Icecream Sandwich",
            "Android Honeycomb",
            "Android Gingerbread",
            "Android Froyo",
            "Android Eclair",
            "Android Donut",
            "Android Cupcake"
    };

    int[] icons = new int[]{
            R.drawable.q,
            R.drawable.pie,
            R.drawable.oreo,
            R.drawable.nougat,
            R.drawable.marshmallow,
            R.drawable.lollipop,
            R.drawable.kitkat,
            R.drawable.jellybean,
            R.drawable.icecreamsw,
            R.drawable.android,
            R.drawable.android,
            R.drawable.android,
            R.drawable.android,
            R.drawable.android,
            R.drawable.android
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initui();
    }

    private void initui(){
        List<HashMap<String,String>> aList = new ArrayList<HashMap<String,String>>();

        for(int i=0;i<15;i++){
            HashMap<String, String> hm = new HashMap<String,String>();
            hm.put("txt", versions[i]);
            hm.put("flag", Integer.toString(icons[i]) );
            aList.add(hm);
        }

        // Keys used in Hashmap
        String[] from = { "flag","txt"};

        // Ids of views in listview_layout
        int[] to = { R.id.flag, R.id.txt};

        // Instantiating an adapter to store each items
        // R.layout.listview_layout defines the layout of each item
        SimpleAdapter ad = new SimpleAdapter(getBaseContext(), aList, R.layout.listview_layout, from, to);

        // Getting a reference to listview of main.xml layout file
        ListView listView = ( ListView ) findViewById(R.id.listview);

        // Setting the adapter to the listView
        listView.setAdapter(ad);
    }
}