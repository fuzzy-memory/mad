package com.example.something;


import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Switch;

import androidx.fragment.app.FragmentActivity;

public class MainActivity extends FragmentActivity {
    ImageView mImg1, mImg2;
    Switch mSwitch;
    int flag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initui();
    }

    public void initui(){
        mImg1=(ImageView) findViewById(R.id.imageView1);
        mImg2=(ImageView) findViewById(R.id.imageView2);
        mSwitch=(Switch) findViewById(R.id.switch1);
        flag=0;
        mImg2.setVisibility(View.INVISIBLE);
    }

    public void toggle(View view) {
        toggle_max();
        mSwitch.toggle();
    }

    public void toggle_max() {
        if(flag==0){
            mImg2.setVisibility(View.VISIBLE);
            mImg1.setVisibility(View.INVISIBLE);
            flag=1;
        }
        else{
            mImg2.setVisibility(View.INVISIBLE);
            mImg1.setVisibility(View.VISIBLE);
            flag=0;
        }
    }
}
