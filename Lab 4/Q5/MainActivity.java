package com.example.something;

import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Toast;


import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    CheckBox mCheck1, mCheck2, mCheck3, mCheck4, mCheck5, mCheck6, mCheck7, mCheck8, mCheck9, mCheck10;
    ArrayList<String> finalOrder;
    String empty, finalise;
    float tot;
    boolean togg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initui();

    }

    private void initui(){
        mCheck1=(CheckBox) findViewById(R.id.checkBox1);
        mCheck2=(CheckBox) findViewById(R.id.checkBox2);
        mCheck3=(CheckBox) findViewById(R.id.checkBox3);
        mCheck4=(CheckBox) findViewById(R.id.checkBox4);
        mCheck5=(CheckBox) findViewById(R.id.checkBox5);
        mCheck6=(CheckBox) findViewById(R.id.checkBox6);
        mCheck7=(CheckBox) findViewById(R.id.checkBox7);
        mCheck8=(CheckBox) findViewById(R.id.checkBox8);
        mCheck9=(CheckBox) findViewById(R.id.checkBox9);
        mCheck10=(CheckBox) findViewById(R.id.checkBox10);

        finalOrder=new ArrayList<String>();
        empty="Add an item";
        finalise="";
        togg=true;
        tot=0;
    }

    public void cart(View view) {
        toggle();
        finalOrder.clear();
        finalise="";
        tot=0;
        if(mCheck1.isChecked()) {
            finalOrder.add(mCheck1.getText().toString() + " Rs. 40/-");
            tot += 40;
        }

        if(mCheck2.isChecked()) {
            finalOrder.add(mCheck2.getText().toString() + " Rs. 85/-");
            tot += 85;
        }

        if(mCheck3.isChecked()) {
            finalOrder.add(mCheck3.getText().toString() + " Rs. 95/-");
            tot += 95;
        }

        if(mCheck4.isChecked()) {
            finalOrder.add(mCheck4.getText().toString() + " Rs. 150/-");
            tot += 150;
        }

        if(mCheck5.isChecked()) {
            finalOrder.add(mCheck5.getText().toString() + " Rs. 170/-");
            tot += 170;
        }

        if(mCheck6.isChecked()) {
            finalOrder.add(mCheck6.getText().toString() + " Rs. 230/-");
            tot += 230;
        }

        if(mCheck7.isChecked()) {
            finalOrder.add(mCheck7.getText().toString() + " Rs. 35/-");
            tot += 35;
        }

        if(mCheck8.isChecked()) {
            finalOrder.add(mCheck8.getText().toString() + " Rs. 145/-");
            tot += 145;
        }

        if(mCheck9.isChecked()) {
            finalOrder.add(mCheck9.getText().toString() + " Rs. 80/-");
            tot += 80;
        }

        if(mCheck10.isChecked()) {
            finalOrder.add(mCheck10.getText().toString() + " Rs. 40/-");
            tot += 40;
        }

        for(String x:finalOrder)
            finalise+=x+"\n";
        finalise+="\nTotal cost = Rs "+tot;

        if(finalOrder.isEmpty()) {
            finalise = empty;
            toggle();
        }

        Toast.makeText(this, finalise, Toast.LENGTH_SHORT).show();

    }

    private void toggle(){
        if(!togg)
            togg=true;
        else togg=false;
        mCheck1.setClickable(togg);
        mCheck2.setClickable(togg);
        mCheck3.setClickable(togg);
        mCheck4.setClickable(togg);
        mCheck5.setClickable(togg);
        mCheck6.setClickable(togg);
        mCheck7.setClickable(togg);
        mCheck8.setClickable(togg);
        mCheck9.setClickable(togg);
        mCheck10.setClickable(togg);
    }

    public void clear(View view) {
        toggle();
        mCheck1.setChecked(false);
        mCheck2.setChecked(false);
        mCheck3.setChecked(false);
        mCheck4.setChecked(false);
        mCheck5.setChecked(false);
        mCheck6.setChecked(false);
        mCheck7.setChecked(false);
        mCheck8.setChecked(false);
        mCheck9.setChecked(false);
        mCheck10.setChecked(false);
    }
}
