package com.example.something;


import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.fragment.app.FragmentActivity;

public class MainActivity extends FragmentActivity {
    ImageView mImg;
    ToggleButton mToggle;
    boolean state;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initui();
    }

    public void initui(){
        mImg=(ImageView) findViewById(R.id.imageView1);
        mImg.setVisibility(View.INVISIBLE);
        mToggle=(ToggleButton) findViewById(R.id.toggleButton1);
        mToggle.setText("Ringing");
        mToggle.setChecked(false);
        mToggle.setTextOff("Ringing");
        mToggle.setTextOn("Silent");
        state=false;
    }

    public void togg(View view) {
        if(mToggle.isChecked())
            doSilent();
        else doRing();
    }

    public void togg2(View view) {
        state=mToggle.isChecked();
        if(!state)
            doSilent();
        else doRing();
        mToggle.toggle();
    }

    public void doSilent(){
        Toast.makeText(this,"Silent", Toast.LENGTH_SHORT).show();
        mImg.setImageResource(R.drawable.silent);
        mImg.setVisibility(View.VISIBLE);
        mImg.postDelayed(new Runnable() {
            @Override
            public void run() {
                mImg.setVisibility(View.INVISIBLE);
            }
        }, 2000);
    }

    public void doRing(){
        Toast.makeText(this,"Ringing", Toast.LENGTH_SHORT).show();
        mImg.setImageResource(R.drawable.ring);
        mImg.setVisibility(View.VISIBLE);
        mImg.postDelayed(new Runnable() {
            @Override
            public void run() {
                mImg.setVisibility(View.INVISIBLE);
            }
        }, 2000);
    }
}
