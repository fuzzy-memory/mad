package com.example.something;


import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.Toast;

import androidx.fragment.app.FragmentActivity;

public class MainActivity extends FragmentActivity {
    ImageView mImg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initui();
    }

    public void initui(){
        mImg=(ImageView) findViewById(R.id.imageView1);
        mImg.setVisibility(View.INVISIBLE);
    }

    public void click(View view) {
        switch (view.getId()){
            case R.id.button1:
                Toast.makeText(this, "Android Q", Toast.LENGTH_SHORT).show();
                mImg.setImageResource(R.drawable.q);
                mImg.setVisibility(View.VISIBLE);
                mImg.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mImg.setVisibility(View.INVISIBLE);
                    }
                }, 2000);
                break;
            case R.id.button2:
                Toast.makeText(this, "Android Pie", Toast.LENGTH_SHORT).show();
                mImg.setImageResource(R.drawable.pie);
                mImg.setVisibility(View.VISIBLE);
                mImg.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mImg.setVisibility(View.INVISIBLE);
                    }
                }, 2000);
                break;
            case R.id.button3:
                Toast.makeText(this, "Android Oreo", Toast.LENGTH_SHORT).show();
                mImg.setImageResource(R.drawable.oreo);
                mImg.setVisibility(View.VISIBLE);
                mImg.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mImg.setVisibility(View.INVISIBLE);
                    }
                }, 2000);
                break;
            case R.id.button4:
                Toast.makeText(this, "Android Nougat", Toast.LENGTH_SHORT).show();
                mImg.setImageResource(R.drawable.nougat);
                mImg.setVisibility(View.VISIBLE);
                mImg.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mImg.setVisibility(View.INVISIBLE);
                    }
                }, 2000);
                break;
            case R.id.button5:
                Toast.makeText(this, "Android Marshmallow", Toast.LENGTH_SHORT).show();
                mImg.setImageResource(R.drawable.marshmallow);
                mImg.setVisibility(View.VISIBLE);
                mImg.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mImg.setVisibility(View.INVISIBLE);
                    }
                }, 2000);
                break;
        }
    }
}
